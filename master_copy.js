
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);


var bgReady = false;
var bgImage = new Image();
bgImage.onload = function () {
        bgReady = true;
};
bgImage.src = "images/background.png";


var heroReady = false;
var heroImage = new Image();
heroImage.onload = function () {
        heroReady = true;
};
heroImage.src = "images/hero.png";


var goblinReady = false;
var goblinImage = new Image();
goblinImage.onload = function () {
        goblinReady = true;
};
goblinImage.src = "images/goblin.png";

var lifeReady = false;
var lifeImage = new Image();
lifeImage.onload = function () {
        lifeReady = true;
};
lifeImage.src = "images/life.png";

var wrapperReady = false;
var wrapperImage = new Image();
wrapperImage.onload = function () {
        wrapperReady = true;
};
wrapperImage.src = "images/wrapper.png";

var hero = {
    speed: 200,
    lives: 5,
    canWrap: true 
}

var goblin = {
    speed: 50
};

var life = {};

var level = 1;

var keysDown = {};

var wrapper = {};

addEventListener("keydown", function (e) {
    keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
    delete keysDown[e.keyCode];
}, false);

var reset = function() {
    resetGoblin();
    resetLife();
    resetWrapper();    
    hero.canWrap = true;
};


var resetHero = function () {
    hero.x = canvas.width / 2;
    hero.y = canvas.height / 2;
    
};

var resetGoblin = function() {
    goblin.x = 32 + (Math.random() * (canvas.width - 64));
    goblin.y = 32 + (Math.random() * (canvas.height - 64));
};

var resetLife = function() {
    life.x = 32 + (Math.random() * (canvas.width - 64));
    life.y = 32 + (Math.random() * (canvas.height - 64));
}

var resetWrapper = function() {
    wrapper.exists = false;
    if(Math.random() <= 0.5){
        wrapper.exists = true;
        wrapper.x = 32 + (Math.random() * (canvas.width - 64));
        wrapper.y = 32 + (Math.random() * (canvas.height - 64));

    }
}

var update = function (modifier) {
        
    if (38 in keysDown) { 
        hero.y = Math.max((hero.y - (hero.speed * modifier)), 0);
        if(hero.canWrap && hero.y == 0){hero.y = canvas.height - 32};
    }
    if (40 in keysDown) {
        hero.y = Math.min((hero.y + (hero.speed * modifier)), (canvas.height - 32));
        if(hero.canWrap){hero.y %= (canvas.height - 32)};
    }
    if (37 in keysDown) {
        hero.x = Math.max((hero.x - (hero.speed * modifier)), 0);
        if(hero.canWrap && hero.x == 0){hero.x = canvas.width - 32};

    }
    if (39 in keysDown) { 
        hero.x = Math.min((hero.x + (hero.speed * modifier)), (canvas.width - 32));
        if(hero.canWrap){hero.x %= (canvas.width - 32)};

    }
    
    if(goblin.x > hero.x){
        goblin.x -= Math.min((goblin.speed * modifier), (goblin.x - hero.x));
    } else if(goblin.x < hero.x){
        goblin.x += Math.min((goblin.speed * modifier), (hero.x - goblin.x));
    }
    
    if(goblin.y > hero.y){
        goblin.y -= Math.min((goblin.speed * modifier), (goblin.y - hero.y));
    } else if(goblin.y < hero.y){
        goblin.y += Math.min((goblin.speed * modifier), (hero.y - goblin.y));
    }

    var heroCollision = (hero.x <= (goblin.x + 32)
                && goblin.x <= (hero.x + 32)
                && hero.y <= (goblin.y + 32)
                && goblin.y <= (hero.y + 32));

    var lifeCollision = (hero.x <= (life.x + 32)
                && life.x <= (hero.x + 32)
                && hero.y <= (life.y + 32)
                && life.y <= (hero.y + 32));

    var wrapperCollision = (hero.x <= (wrapper.x + 32)
                && wrapper.x <= (hero.x + 32)
                && hero.y <= (wrapper.y + 32)
                && wrapper.y <= (hero.y + 32));


    if (heroCollision) {
        hero.lives--;
        reset();
    }
    if (lifeCollision) {
        hero.lives++;
        level++;
        goblin.speed += 10;
        resetLife();
        resetWrapper();
    }

    if(wrapper.exists){
        if (wrapperCollision) {
            hero.canWrap = true;
            wrapper.exists= false;
            resetLife();
        };   
    }

};



var render = function () {
      
            
        ctx.fillStyle = "rgb(250, 250, 250)";
        ctx.font = "24px Helvetica";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText("Lives left: " + hero.lives, 32, 32);
        ctx.textAlign = "right";
        ctx.fillText("Level: " + level, canvas.width -32 , 32);







	  if (bgReady) {
                ctx.drawImage(bgImage, 0, 0);
        }

        if (heroReady) {
                ctx.drawImage(heroImage, hero.x, hero.y);
        }

        if (goblinReady) {
                ctx.drawImage(goblinImage, goblin.x, goblin.y);
        }

        if (lifeReady) {
            ctx.drawImage(lifeImage, life.x, life.y);
        }

        if(wrapperReady && wrapper.exists){ 
            ctx.drawImage(wrapperImage, wrapper.x, wrapper.y);
        }

        ctx.fillStyle = "rgb(250, 250, 250)";
        ctx.font = "24px Helvetica";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText("Lives left: " + hero.lives, 32, 32);
        ctx.textAlign = "right";
        ctx.fillText("Level: " + level, canvas.width -32 , 32);
		ctx.font = "20px Georgia";
        ctx.fillText("Ethan Davsion Pearce Test Game", 400, 445);
		
};

var gameOver = function () {

    ctx.fillStyle = "rgb(250, 250, 250)";
    ctx.font = "18px Helvetica";
    ctx.textAlign = "center";
    ctx.textBaseline = "center";
    ctx.fillText("Game Over! Press backspace to play again! ", 200, 200);
    
};

var main = function () {
    var now = Date.now();
    var delta = now - then;

    var newGame = true;

    if(hero.lives <= 0){
        newGame = false;
        gameOver();
        if(8 in keysDown){
            goblin.speed = 50;
            level = 1;
            newGame = true;
            hero.lives = 5;
        }
    } 
    
    if (newGame) {
        update(delta / 1000);
        render();
        then = now;
    }
};


reset();
resetHero();
var then = Date.now();
setInterval(main, 1); 
